package com.demo.camera;

public class PhotoCamera{

    ImageSensor sensor;
    Card card;
    public boolean isActive;


    public PhotoCamera(boolean isActive) {
        this.isActive = isActive;
    }

    public PhotoCamera(ImageSensor sensor) {
        this.sensor = sensor;
    }

    public PhotoCamera(ImageSensor sensor, Card card) {
        this.sensor = sensor;
        this.card = card;

    }

    public void turnOn() {
        sensor.turnOn();

    }

    public void turnOff() {
        sensor.turnOff();
    }


   /* public void pressButton() {
        if (isActive == true) {
            byte[] data = sensor.read();
        }
    }
*/
    public byte[] pressButton() {
        if (isActive == true) {

            card.write(sensor.read());
            return sensor.read();
        }else
            return null;
    }


}

