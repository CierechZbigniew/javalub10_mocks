package com.demo.camera;

import java.util.Arrays;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;

public class PhotoCameraTest {

    @Test
    public void turnOnCameraShouldTurnOnSenor() {

        ImageSensor sensor = mock(ImageSensor.class);

        //boolean isActive = true;
        PhotoCamera photoCamera = new PhotoCamera(sensor);

        photoCamera.turnOn();

        Mockito.verify(sensor).turnOn();
    }

    @Test
    public void turnOffCameraShouldTurnOffSenor() {

        ImageSensor sensor = mock(ImageSensor.class);

        //boolean isActive = false;
        PhotoCamera photoCamera = new PhotoCamera(sensor);

        photoCamera.turnOff();

        Mockito.verify(sensor).turnOff();
    }

    @Test
    public void IfPhotoCameraIsTurnOffPPressButtonDoNothing() {
        ImageSensor sensor = mock(ImageSensor.class);
        PhotoCamera photoCamera = new PhotoCamera(sensor);
        Assertions.assertFalse(photoCamera.isActive);

        photoCamera.pressButton();

        Mockito.verifyZeroInteractions(sensor);
    }

    @Test
    public void IfPhotoCameraIsTurnOnPressButtonCoppyDataFromSensorToCard() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        PhotoCamera photoCamera = new PhotoCamera(sensor, card);
        byte[] data = "data".getBytes();
        Assertions.assertTrue(photoCamera.isActive);

        photoCamera.pressButton();

        Mockito.when(sensor.read()).thenReturn(data);

        Mockito.verify(card).write(data);
    }

}